library(shiny)
library(taxize)
library(dplyr,  warn.conflicts = FALSE)
library(genbankr)
library(rentrez)
library(usethis)
library(tm, warn.conflicts = FALSE)
library(NLP, warn.conflicts = FALSE)
library(dplyr, warn.conflicts = FALSE)
library(parallel)
library(foreach, warn.conflicts = FALSE)
library(doParallel)
#library(purrr, warn.conflicts = FALSE)
library(RVenn)
library(shinycssloaders)
library(UpSetR, warn.conflicts = FALSE)
library(ggplot2, warn.conflicts = FALSE)
library(grid)

making_corpus <- function(trit){
    title_abstract <- paste0(trit$title, trit$abstract, sep=" ")
    tabstract <- as.data.frame(title_abstract, stringsAsFactors = FALSE)
    
    
    trit_merged <- bind_cols(trit, tabstract) %>% 
      select(-abstract) %>%
      rename(text=title_abstract) 
    
    trit_reordered <- trit_merged %>%
      mutate(doc_id = c(1:dim(trit_merged)[1])) %>%
      select(doc_id,text, pmid, title,year, journal)
    
    trit_source <- DataframeSource(trit_reordered)
    trit_corpus <- VCorpus(trit_source)
    
    sapply(trit, class)
    meta(trit_corpus, tag = "pmid", type = "local") <- trit$pmid
    meta(trit_corpus, tag = "description", type = "local") <- trit$title
    meta(trit_corpus, tag = "year", type = "local") <- trit$year
    meta(trit_corpus, tag = "journal", type = "local") <- trit$journal
    
    clean_corpus <- function(corpus){
      corpus <- tm_map(corpus, content_transformer(tolower))
      corpus <- tm_map(corpus, removePunctuation)
      corpus <- tm_map(corpus, removeNumbers)
      corpus <- tm_map(corpus, removeWords, words = c(stopwords("en"), "known", "activated", "increasing", "significant", "investigated", "applied", "concentration", "study", "showed", "improved", "enhanced", "two", "using", "results", "also", "increase", "use", "active"))
      corpus <- tm_map(corpus, stripWhitespace)
      return(corpus)
    }
    
    trit_corpus <- clean_corpus(trit_corpus)
    return(trit_corpus)
}

# rds <- readRDS("try.rds")
# corpus_ready <- making_corpus(rds)
# ext <- as.data.frame(meta(corpus_ready))
# ext %>% group_by(year) %>%
#   summarise(cnt=n())
# corpus_stemmed <- tm_map(corpus_ready, stemDocument, language = "english")
# 
# corpus_tdm <- TermDocumentMatrix(corpus_stemmed)
# corpus_m <- as.matrix(corpus_tdm)
# 
# corpus_m <- rowSums(corpus_m)
# corpus_m <- sort(corpus_m, decreasing =TRUE)
# 
# data <- data.frame("x"=corpus_m[1:10])
# data$words <- names(corpus_m)[1:10]
# ggplot(data, aes(words, corpus_m.1.10.))+
#   geom_bar(stat="identity")+
#   xlab("Frequency")+
#   coord_flip()
# 
# 
# 
# extra <- as.data.frame(meta(rds_c))
# 
# searchwords <- c("anther", "fertility", "floral", "floret", "flower", "inflorescence", "lemma", "palea", "panicle", "pistil", "pollen", "spike", "stamen", "stigma", "style","wheat", "barley", "triticeae")
# a <- foreach(i=searchwords, .packages = "tm") %do% {
#   meta(tm_filter(rds_c, FUN = function(x) any(grep(i, content(x)))))$pmid}
# names(a) <- searchwords
# 
# fullset <- list(meta(rds_c)$pmid)
# names(fullset) <- "PubmedResults"
# 
# listInput <- append(fullset, a)
# u <- fromList(listInput)
# str(u)
# colnames(u) <- names(u)
# 
# all <- bind_cols(extra, u)
# all$year = as.numeric(all$year)
# upset(u, sets = searchwords,
#       nintersects = 50, mb.ratio = c(0.55, 0.45), order.by = "freq", keep.order = TRUE, number.angles = 30, text.scale = c(1.3, 1.3, 1, 1, 1, 0.75), 
#       main.bar.color = "black", matrix.color = "forestgreen", sets.bar.color = "darkgreen", shade.color = "palegreen", matrix.dot.alpha = 60, shade.alpha = 0.75,
#       mainbar.y.label = "Intersection Size", sets.x.label = "Articles per searchword")
